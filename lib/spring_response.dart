import 'server_error_response.dart';
import 'form_error_response.dart';

class SpringResponse<T> {

  final int statusCode;
  final bool isSuccessful;
  final bool isFormError;
  final bool isServerError;
  final bool isOtherErrors;
  final bool isUnauthorized;

  final String rawBody;
  final T body;

  final List<FormErrorResponse> errors;

  SpringResponse({
    this.statusCode,
    this.isSuccessful = false,
    this.isFormError = false,
    this.isServerError = false,
    this.isOtherErrors = false,
    this.isUnauthorized = false,
    this.rawBody,
    this.body,
    this.errors});

  List<String> get errorsText => errors.map((item) => item.defaultMessage).toList();
}