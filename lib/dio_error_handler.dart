import 'package:dio/dio.dart';

import 'form_error_response.dart';
import 'spring_response.dart';

class SpringErrorHandler {

  SpringResponse<T> handleError<T>(Exception error) {
    if(error is DioError) {
      if(error.type == DioErrorType.RESPONSE) {

        if (error.response.statusCode == 422) {
          if(error.response.data != null) {
            return SpringResponse(
              statusCode: error.response.statusCode,
              isFormError: true,
              errors: FormErrorResponse.fromJsonList(error.response.data));
          }

        } else if(error.response.statusCode == 401) {
          return SpringResponse(
            statusCode: error.response.statusCode,
            isUnauthorized: true,
            errors: [ FormErrorResponse(defaultMessage: "Você não tem autorização para realizar esta ação") ]
          );
        }
        else if (error.response.statusCode == 500) {

          return SpringResponse(
            statusCode: error.response.statusCode,
            isServerError: true,
            errors: [ FormErrorResponse(defaultMessage: "Não foi possivel completar a ação") ]//ServerErrorResponse.fromJson(e.response.data)
          );

        }
        return SpringResponse(
          statusCode: error.response.statusCode,
          isOtherErrors: true,
          errors: [ FormErrorResponse(defaultMessage: "Não foi possivel completar a ação") ]
        );
      }
    }
    print('ERRO: $error');
    return SpringResponse(
      isOtherErrors: true,
      errors: [FormErrorResponse(defaultMessage: "Houve um erro na aplicação")]
    );
  }
}