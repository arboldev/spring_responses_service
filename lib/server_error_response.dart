class ServerErrorResponse {

  final int timestamp;
  final int status;
  final String error;

  ServerErrorResponse({ this.timestamp, this.status, this.error });

  factory ServerErrorResponse.fromJson(Map<String, dynamic> json) {
    return ServerErrorResponse(
      timestamp: json['timestamp'],
      status: json['status'],
      error: json['error']
    );
  }


}