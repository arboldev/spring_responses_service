import 'sort.dart';

class SpringList<T> {

  final int size;
  final int number;
  final int totalElements;
  final bool last;
  final int totalPages;
  final Sort sort;
  final bool first;
  final int numberOfElements;

  final List<T> content;

  SpringList({
    this.size,
    this.number,
    this.totalElements,
    this.last,
    this.totalPages,
    this.sort,
    this.first,
    this.numberOfElements,
    this.content
  });

   factory SpringList.fromJson(Map<String, dynamic> json, List<T> content) {
    return SpringList(
      size: json['size'],
      number: json['number'],
      totalElements: json['totalElements'],
      last: json['last'],
      totalPages: json['totalPages'],
      sort: Sort.toJson(json['sort']),
      numberOfElements: json['numberOfElements'],
      content: content
    );
  }

}