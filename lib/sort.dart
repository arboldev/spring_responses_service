class Sort {

  final bool sorted;
  final bool unsorted;
  final bool empty;

  Sort({ this.sorted, this.unsorted, this.empty });

  factory Sort.toJson(Map<String, dynamic> json) {
    return Sort(
      sorted: json['sorted'],
      unsorted: json['unsorted'],
      empty: json['empty'],
    );
  }


}