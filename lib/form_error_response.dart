class FormErrorResponse {

  final List<dynamic> codes;
  final String defaultMessage;
  final String objectName;

  FormErrorResponse({ this.codes, this.defaultMessage, this.objectName });

  factory FormErrorResponse.fromJson(Map<String, dynamic> json) {
    return FormErrorResponse(
      codes: json['codes'],
      defaultMessage: json['defaultMessage'].toString(),
      objectName: json['objectName']
    );
  }

  static List<FormErrorResponse> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((item) {
      final error = FormErrorResponse.fromJson(item);
      return error;
    }).toList();
  }
}